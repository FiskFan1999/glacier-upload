# glacierupload

Upload files into amazon glacier using the golang SDK.

Based on example in https://docs.aws.amazon.com/sdk-for-go/v1/developer-guide/using-glacier-with-go-sdk.html