package main

import (
	"io"
	"log"
	"os"

	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/glacier"

	pb "github.com/cheggaaa/pb/v3"
)

type PBReadSeeker struct {
	Reader io.ReadSeeker
	bar    *pb.ProgressBar
}

func NewProxyReadSeeker(p *pb.ProgressBar, r io.ReadSeeker) *PBReadSeeker {
	return &PBReadSeeker{r, p}
}

func (r *PBReadSeeker) Read(p []byte) (n int, err error) {
	n, err = r.Reader.Read(p)
	r.bar.Add(n)
	return
}

func (r *PBReadSeeker) Seek(offset int64, whence int) (result int64, err error) {
	/*
		Note: `result` is the offset relative to the file
		(array index)
	*/
	result, err = r.Reader.Seek(offset, whence)
	r.bar.SetCurrent(result)
	return
}

// Close the reader when it implements io.Closer
func (r *PBReadSeeker) Close() (err error) {
	r.bar.Finish()
	if closer, ok := r.Reader.(io.Closer); ok {
		return closer.Close()
	}
	return
}

func main() {
	if len(os.Args) != 3 {
		log.Printf("%s <vault name> <filename>", os.Args[0])
		os.Exit(1)
	}
	vaultName := os.Args[1]
	filename := os.Args[2]

	file, err := os.Open(filename)
	if err != nil {
		log.Fatalf("%s", err.Error()) // exits
	}

	fileStats, err := file.Stat()
	if err != nil {
		log.Fatalf("%s", err.Error()) // exits
	}

	bar := pb.Full.Start64(fileStats.Size())
	bar.Set(pb.Bytes, true)

	fileReader := NewProxyReadSeeker(bar, file)

	// Initialize a session that the SDK uses to load
	// credentials from the shared credentials file ~/.aws/credentials
	// and configuration from the shared configuration file ~/.aws/config.
	sess := session.Must(session.NewSessionWithOptions(session.Options{
		SharedConfigState: session.SharedConfigEnable,
	}))

	// Create Glacier client in default region
	svc := glacier.New(sess)

	var fileStatsName string = fileStats.Name()

	result, err := svc.UploadArchive(&glacier.UploadArchiveInput{
		VaultName:          &vaultName,
		Body:               fileReader,
		ArchiveDescription: &fileStatsName,
	})
	if err != nil {
		log.Println("Error uploading archive.", err)
		return
	}

	log.Println("Uploaded to archive", *result.ArchiveId)
	// end snippet
}
